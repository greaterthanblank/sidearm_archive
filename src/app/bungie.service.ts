import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../environments/environment';
import { tap, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BungieService {

  constructor(private http: HttpClient) { }

  private apiHttpOpts: any;
  private token: Token;

  public manifest: any;

  public membership: MembershipInfo;
  public characterId: BehaviorSubject<number>;

  fetchToken(code: string): Observable<TokenResp> {
    const body = new URLSearchParams();
    body.append('grant_type', 'authorization_code');
    body.append('code', code);
    body.append('client_id', environment.clientId.toString());
    body.append('client_secret', environment.clientSecret.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    }

    return this.http.post<TokenResp>('https://www.bungie.net/platform/app/oauth/token/', body.toString(), httpOptions).pipe(
      tap(resp => {
        this.token = new Token(resp);
        localStorage.setItem('token', JSON.stringify(this.token));
        this.apiHttpOpts = {
          headers: new HttpHeaders({
            'X-API-Key': environment.apiKey,
            'Authorization': 'Bearer ' + this.token.access_token
          })
        };
      })
    )
  }

  needLogin() {
    let token: Token = JSON.parse(localStorage.getItem('token'));
    if (token) {
      if (token.refresh_expires_at - 3600000 < Date.now()) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  refreshToken() {
    const body = new URLSearchParams();
    let oldToken: Token = JSON.parse(localStorage.getItem('token'));
    body.append('grant_type', 'refresh_token');
    body.append('refresh_token', oldToken.refresh_token);
    body.append('client_id', environment.clientId.toString());
    body.append('client_secret', environment.clientSecret.toString());

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    }

    return this.http.post<TokenResp>('https://www.bungie.net/platform/app/oauth/token/', body.toString(), httpOptions).pipe(
      tap(resp => {
        this.token = new Token(resp);
        localStorage.setItem('token', JSON.stringify(this.token));
        this.apiHttpOpts = {
          headers: new HttpHeaders({
            'X-API-Key': environment.apiKey,
            'Authorization': 'Bearer ' + this.token.access_token
          })
        };
      })
    )
  }

  getInventory(profile: number, platform: number): Observable<any> {
    return this.http.get<any>('https://www.bungie.net/Platform/Destiny2/' + platform + '/Profile/' + profile + '/?components=102,103,200,201,202,205,300,301,304,305,306,307,308,800', this.apiHttpOpts);
  }

  getVendors(profile: number, platform: number, char: number): Observable<any> {
    return this.http.get<any>('https://www.bungie.net/Platform/Destiny2/' + platform + '/Profile/' + profile + '/Character/' + char + '/Vendors/?components=400,401,402', this.apiHttpOpts);
  }

  loadMembershipDefaults(): Observable<any> {
    return this.getMemberships().pipe(
      mergeMap(resp => {
        let primaryAccount = resp.Response.destinyMemberships.find(acc => acc.membershipId == resp.Response.primaryMembershipId)
        this.membership = new MembershipInfo(primaryAccount.membershipId, primaryAccount.membershipType)
        return this.getCharacters(this.membership.membershipId, this.membership.membershipType);  
      }),
      tap(resp => {
        let memberResp = resp;
        let mostRecentChar: number = null;
        let mostRecentTime: Date = null;
        for (const [key, value] of Object.entries<any>(memberResp.Response.characters.data)) {
          let lastPlayed: Date = new Date(value.dateLastPlayed);
          if (lastPlayed > mostRecentTime) {
            mostRecentTime = lastPlayed;
            mostRecentChar = value.characterId;
          }
        }
        this.characterId = new BehaviorSubject<number>(mostRecentChar);
      })
    )
  }

  getMemberships(): Observable<any> {
    return this.http.get<any>('https://www.bungie.net/Platform/User/GetMembershipsById/' + this.token.membership_id + '/254/', this.apiHttpOpts);
  }

  getCharacters(profile: number, platform: number): Observable<any> {
    return this.http.get<any>('https://www.bungie.net/Platform/Destiny2/' + platform + '/Profile/' + profile + '/?components=200', this.apiHttpOpts);
  }

  getLatestManifest(): Observable<any> {
    return this.http.get<any>('https://www.bungie.net/Platform/Destiny2/Manifest/', this.apiHttpOpts).pipe(
      mergeMap((resp: any) => {
        return this.http.get<any>('https://www.bungie.net' + resp.Response.jsonWorldContentPaths.en);
      }),
      tap((manifest: any) => {
        this.manifest = manifest;
      })
    )
  }
}

interface TokenResp {
  access_token: string,
  expires_in: number,
  membership_id: string,
  refresh_expires_in: number,
  refresh_token: string,
  token_type: string
}

class Token {
  public access_token: string;
  public expires_at: number;
  public membership_id: string;
  public refresh_expires_at: number;
  public refresh_token: string;
  constructor(resp: TokenResp) {
    this.access_token = resp.access_token;
    this.membership_id = resp.membership_id;
    this.refresh_token = resp.refresh_token;
    let now: number = Date.now();
    this.expires_at = now + (resp.expires_in * 1000);
    this.refresh_expires_at = now + (resp.refresh_expires_in * 1000);
  }
}

class MembershipInfo {
  public membershipId: number;;
  public membershipType: number;
  constructor(id: number, type: number) {
    this.membershipId = id;
    this.membershipType = type;
  }
}