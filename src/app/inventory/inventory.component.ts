import { Component, OnInit } from '@angular/core';
import { BungieService } from '../bungie.service';
import { mergeMap, zip, tap, map } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  constructor(private bungie: BungieService) { }

  private inventories: any = [];

  ngOnInit() {
    this.bungie.getMemberships().pipe(
      mergeMap(resp => {
        let primaryAccount = resp.Response.destinyMemberships.find(acc => acc.membershipId == resp.Response.primaryMembershipId)
        return this.bungie.getInventory(primaryAccount.membershipId, primaryAccount.membershipType);
      })
    ).subscribe(resp => {
      console.log(resp);
    })
  }

}
