import { Component, OnInit } from '@angular/core';
import { BungieService } from '../bungie.service';
import { mergeMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent implements OnInit {

  constructor(private bungie: BungieService, private route: ActivatedRoute) { }

  public vendors: any;

  public selectedVendor: number;

  public loading: boolean = true;

  ngOnInit(): void {
    this.bungie.characterId.pipe(
      mergeMap((char: number) => {
        return this.bungie.getVendors(this.bungie.membership.membershipId, this.bungie.membership.membershipType, char)
      })
    ).subscribe((resp: any) => {
      this.vendors = resp.Response;
      console.log(this.vendors);
      this.loading = false;
    })

    this.route.params.subscribe(params => {
      this.selectedVendor = params['vendorHash'];
    })
  }

}
