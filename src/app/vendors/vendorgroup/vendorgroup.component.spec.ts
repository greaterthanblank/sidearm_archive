import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorgroupComponent } from './vendorgroup.component';

describe('VendorgroupComponent', () => {
  let component: VendorgroupComponent;
  let fixture: ComponentFixture<VendorgroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorgroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorgroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
