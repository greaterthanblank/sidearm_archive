import { Component, OnInit, Input } from '@angular/core';
import { BungieService } from 'src/app/bungie.service';

@Component({
  selector: 'app-vendorgroup',
  templateUrl: './vendorgroup.component.html',
  styleUrls: ['./vendorgroup.component.scss']
})
export class VendorgroupComponent implements OnInit {

  @Input() group: number;
  @Input() vendorlist: [number];

  constructor(public bungie: BungieService) { }

  ngOnInit(): void {
  }

}
