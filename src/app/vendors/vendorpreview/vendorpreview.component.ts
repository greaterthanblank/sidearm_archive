import { Component, OnInit, Input } from '@angular/core';
import { BungieService } from 'src/app/bungie.service';

@Component({
  selector: 'app-vendorpreview',
  templateUrl: './vendorpreview.component.html',
  styleUrls: ['./vendorpreview.component.scss']
})
export class VendorpreviewComponent implements OnInit {

  @Input() vendor;

  constructor(public bungie: BungieService) { }

  ngOnInit(): void {
  }

}
