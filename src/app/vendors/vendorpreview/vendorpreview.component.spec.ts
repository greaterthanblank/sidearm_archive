import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorpreviewComponent } from './vendorpreview.component';

describe('VendorpreviewComponent', () => {
  let component: VendorpreviewComponent;
  let fixture: ComponentFixture<VendorpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
