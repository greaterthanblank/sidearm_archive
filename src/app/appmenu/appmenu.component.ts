import { Component, OnInit } from '@angular/core';
import { BungieService } from '../bungie.service';
import { Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-appmenu',
  templateUrl: './appmenu.component.html',
  styleUrls: ['./appmenu.component.scss']
})
export class AppmenuComponent implements OnInit {

  public loading = true;

  private membershipId: number = null;
  private membershipType: number = null;
  private characterId: number = null;

  constructor(private bungie: BungieService, private router: Router) { }

  ngOnInit() {
    if (this.bungie.needLogin()) {
      this.router.navigate(['login']);
    } else {
      this.bungie.refreshToken().pipe(
        mergeMap(() => {
          return forkJoin([this.bungie.loadMembershipDefaults(), this.bungie.getLatestManifest()])
        })
      ).subscribe(() => {
        this.loading = false;
      })
    }


  }

}
