import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RedirectComponent } from './redirect/redirect.component';
import { AppmenuComponent } from './appmenu/appmenu.component';
import { InventoryComponent } from './inventory/inventory.component';
import { VendorsComponent } from './vendors/vendors.component';


const routes: Routes = [
  { path: '', redirectTo: '/app', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'redirect', component: RedirectComponent },
  {
    path: 'app',
    component: AppmenuComponent,
    children: [
      {
        path: 'inventory',
        component: InventoryComponent
      }, {
        path: 'vendors',
        component: VendorsComponent,
      }, {
        path: 'vendors/:vendorHash',
        component: VendorsComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
